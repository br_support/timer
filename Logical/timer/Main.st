
PROGRAM _INIT

	 
END_PROGRAM

PROGRAM _CYCLIC
	CASE step OF
		0:	//vypnuto
			do1 := FALSE;
			TON_0(IN := TRUE, PT := T#10s);
			IF TON_0.Q THEN
				TON_0(IN := FALSE);
				step := step + 1;
			END_IF
		
		1:	//zapnuto
			do1 := TRUE;
			TON_0(IN := TRUE, PT := T#3m);
			IF TON_0.Q THEN
				TON_0(IN := FALSE);
				step := step + 1;
			END_IF
		
		2:
			cycles := cycles + 1;
			step := 0;			
		
	END_CASE;
	 
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

